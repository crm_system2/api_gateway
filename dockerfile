FROM golang:alpine

RUN mkdir market

EXPOSE 8080

ADD . /market

WORKDIR /market

RUN go build -o main cmd/main.go

CMD ["./main"]